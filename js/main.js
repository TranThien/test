import {
  renderToDo,
  renderToDoCompleted,
  saveLocalStorage,
  sortAZ,
  sortZA,
} from "./model/model.js";

let myArrayList = [];
let myArrayComplete = [];
// lấy dữ liệu từ local lên
let dataJson = localStorage.getItem("DS");

if (dataJson) {
  var dataRaw = JSON.parse(dataJson);
  // convert từ json -> chuỗi
  renderToDo(dataRaw);
}

document.getElementById("addItem").addEventListener("click", function () {
  let inputElement = document.getElementById("newTask");
  if (inputElement.value == "") {
    return;
  }
  myArrayList.push(inputElement.value);
  // //lưu xuống local và convert từ string sang JSOn
  saveLocalStorage(myArrayList);
  renderToDo(myArrayList);
  inputElement.value = "";
});

let xoaItem = (index) => {
  myArrayList.splice(index, 1);
  saveLocalStorage(myArrayList);
  renderToDo(myArrayList);
};
window.xoaItem = xoaItem;

let checkItem = (index) => {
  myArrayComplete.push(myArrayList.splice(index, 1));
  renderToDo(myArrayList);
  saveLocalStorage(myArrayComplete);
  renderToDoCompleted(myArrayComplete);
};
window.checkItem = checkItem;
let xoaItemCompleted = (index) => {
  myArrayComplete.splice(index, 1);
  saveLocalStorage(myArrayComplete);
  renderToDoCompleted(myArrayComplete);
};
window.xoaItemCompleted = xoaItemCompleted;
// sort A-z
document.getElementById("two").addEventListener("click", () => {
  sortAZ(myArrayList);
});
// sort Z-A
document.getElementById("three").addEventListener("click", () => {
  sortZA(myArrayList);
});
